﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace Timerbypass
{
    public partial class TimerBypass : Form
    {
        Process[] p;
        Thread sendMacroThread;
        int index = 0;
        uint WM_KEYDOWN = 0x0100;
        uint WM_KEYUP = 0x0101;
        public volatile bool running = false;

        [DllImport("user32.dll")]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, Keys wParam, IntPtr lParam);

        public TimerBypass()
        {
            InitializeComponent();
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            p = Process.GetProcessesByName("SimplyRO");
            lblStatus.Text = "Found " + p.Length + " processes. Process 1 selected.";
            index = 0;
        }

        private void btnControl_Click(object sender, EventArgs e)
        {
            if (!running)
            {
                sendMacroThread = new Thread(new ThreadStart(SendMacro));
                btnNext.Enabled = false;
                btnScan.Enabled = false;
                btnControl.Text = "Stop";
                sendMacroThread.Start();
            }
            else
            {
                sendMacroThread.Interrupt();
                btnControl.Text = "Start";
                btnNext.Enabled = true;
                btnScan.Enabled = true;
            }
            
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            ++index;
            if (index > p.Length - 1)
            {
                index = 0;
            }
            lblStatus.Text = "Process " + (index + 1) + " selected.";
        }

        public void SendMacro()
        {
            try
            {
                running = true;
                while (running)
                {
                    PostMessage(p[index].MainWindowHandle, WM_KEYDOWN, Keys.Insert, IntPtr.Zero);
                    Thread.Sleep(500);
                    PostMessage(p[index].MainWindowHandle, WM_KEYUP, Keys.Insert, IntPtr.Zero);
                    if (chkEsc.Checked)
                    {
                        Thread.Sleep(5000);
                        PostMessage(p[index].MainWindowHandle, WM_KEYDOWN, Keys.Escape, IntPtr.Zero);
                        Thread.Sleep(500);
                        PostMessage(p[index].MainWindowHandle, WM_KEYUP, Keys.Escape, IntPtr.Zero);
                    }
                    Thread.Sleep(40000);
                }
            }
            catch (ThreadInterruptedException e)
            {
                Console.Write(e.Message);
            }
            finally
            {
                running = false;
            }
        }
    }
}
